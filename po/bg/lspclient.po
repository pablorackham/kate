# Language  translations for kate package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Automatically generated, 2022.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-08 00:49+0000\n"
"PO-Revision-Date: 2022-05-16 21:20+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.0\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:252
#, kde-format
msgid "Filter..."
msgstr "Филтър..."

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:98 lspclientconfigpage.cpp:103
#: lspclientpluginview.cpp:471 lspclientpluginview.cpp:627 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "Клиент на LSP"

#: lspclientconfigpage.cpp:213
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: lspclientconfigpage.cpp:222
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: lspclientconfigpage.cpp:227
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: lspclientconfigpage.cpp:275
#, kde-format
msgid "Delete selected entries"
msgstr ""

#: lspclientconfigpage.cpp:280
#, kde-format
msgid "Delete all entries"
msgstr ""

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr ""

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:438 lspclientpluginview.cpp:691
#, kde-format
msgid "LSP"
msgstr "LSP"

#: lspclientpluginview.cpp:499
#, kde-format
msgid "Go to Definition"
msgstr "Отиване до дефиниция"

#: lspclientpluginview.cpp:501
#, kde-format
msgid "Go to Declaration"
msgstr "Отиване до декларация"

#: lspclientpluginview.cpp:503
#, kde-format
msgid "Go to Type Definition"
msgstr ""

#: lspclientpluginview.cpp:505
#, kde-format
msgid "Find References"
msgstr ""

#: lspclientpluginview.cpp:508
#, kde-format
msgid "Find Implementations"
msgstr ""

#: lspclientpluginview.cpp:510
#, kde-format
msgid "Highlight"
msgstr "Акцентиране"

#: lspclientpluginview.cpp:512
#, kde-format
msgid "Symbol Info"
msgstr ""

#: lspclientpluginview.cpp:514
#, kde-format
msgid "Search and Go to Symbol"
msgstr ""

#: lspclientpluginview.cpp:519
#, kde-format
msgid "Format"
msgstr "Формат"

#: lspclientpluginview.cpp:522
#, kde-format
msgid "Rename"
msgstr "Преименуване"

#: lspclientpluginview.cpp:526
#, fuzzy, kde-format
msgid "Expand Selection"
msgstr "Разгъване на всички"

#: lspclientpluginview.cpp:529
#, kde-format
msgid "Shrink Selection"
msgstr ""

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Switch Source Header"
msgstr ""

#: lspclientpluginview.cpp:536
#, kde-format
msgid "Expand Macro"
msgstr ""

#: lspclientpluginview.cpp:538
#, kde-format
msgid "Code Action"
msgstr ""

#: lspclientpluginview.cpp:553
#, kde-format
msgid "Show selected completion documentation"
msgstr ""

#: lspclientpluginview.cpp:556
#, kde-format
msgid "Enable signature help with auto completion"
msgstr ""

#: lspclientpluginview.cpp:559
#, kde-format
msgid "Include declaration in references"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:562 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr ""

#: lspclientpluginview.cpp:565
#, kde-format
msgid "Show hover information"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:568 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr ""

#: lspclientpluginview.cpp:571
#, kde-format
msgid "Incremental document synchronization"
msgstr ""

#: lspclientpluginview.cpp:574
#, kde-format
msgid "Highlight goto location"
msgstr ""

#: lspclientpluginview.cpp:583
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:587
#, fuzzy, kde-format
#| msgctxt "@title:tab"
#| msgid "Diagnostics"
msgid "Show Diagnostics Notifications"
msgstr "Диагностика"

#: lspclientpluginview.cpp:592
#, fuzzy, kde-format
#| msgid "Show Details"
msgid "Show Messages"
msgstr "Показване на детайли"

#: lspclientpluginview.cpp:597
#, kde-format
msgid "Server Memory Usage"
msgstr ""

#: lspclientpluginview.cpp:601
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr ""

#: lspclientpluginview.cpp:603
#, kde-format
msgid "Restart LSP Server"
msgstr ""

#: lspclientpluginview.cpp:605
#, kde-format
msgid "Restart All LSP Servers"
msgstr ""

#: lspclientpluginview.cpp:616
#, kde-format
msgid "Go To"
msgstr ""

#: lspclientpluginview.cpp:643
#, kde-format
msgid "More options"
msgstr "Повече опции"

#: lspclientpluginview.cpp:859 lspclientsymbolview.cpp:287
#, kde-format
msgid "Expand All"
msgstr "Разгъване на всички"

#: lspclientpluginview.cpp:860 lspclientsymbolview.cpp:288
#, kde-format
msgid "Collapse All"
msgstr "Свиване на всички"

#: lspclientpluginview.cpp:1090
#, kde-format
msgid "RangeHighLight"
msgstr "RangeHighLight"

#: lspclientpluginview.cpp:1424
#, kde-format
msgid "Line: %1: "
msgstr "Ред: %1: "

#: lspclientpluginview.cpp:1576 lspclientpluginview.cpp:1932
#: lspclientpluginview.cpp:2052
#, kde-format
msgid "No results"
msgstr "Няма резултати"

#: lspclientpluginview.cpp:1635
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Определение: %1"

#: lspclientpluginview.cpp:1641
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Декларация: %1"

#: lspclientpluginview.cpp:1647
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Определение на тип: %1"

#: lspclientpluginview.cpp:1653
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Препратки: %1"

#: lspclientpluginview.cpp:1665
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Имплементация: %1"

#: lspclientpluginview.cpp:1678
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Акцентиране: %1"

#: lspclientpluginview.cpp:1702 lspclientpluginview.cpp:1713
#: lspclientpluginview.cpp:1726
#, kde-format
msgid "No Actions"
msgstr "Няма действия"

#: lspclientpluginview.cpp:1717
#, kde-format
msgid "Loading..."
msgstr "Зареждане..."

#: lspclientpluginview.cpp:1809
#, kde-format
msgid "No edits"
msgstr "Няма редактирания"

#: lspclientpluginview.cpp:1890
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Преименуване"

#: lspclientpluginview.cpp:1891
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "Ново име (внимание: не всички препратки могат да бъдат заменени)"

#: lspclientpluginview.cpp:1938
#, fuzzy, kde-format
msgid "Not enough results"
msgstr "Няма резултати"

#: lspclientpluginview.cpp:2008
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr "Не е намерен съответният хедър/източник"

#: lspclientpluginview.cpp:2204 lspclientpluginview.cpp:2241
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr "LSP сървър"

#: lspclientpluginview.cpp:2264
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "Клиент на LSP"

#: lspclientservermanager.cpp:652
#, kde-format
msgid "Restarting"
msgstr "Рестартиране"

#: lspclientservermanager.cpp:652
#, kde-format
msgid "NOT Restarting"
msgstr "Без рестартиране"

#: lspclientservermanager.cpp:653
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr ""

#: lspclientservermanager.cpp:861
#, kde-format
msgid "Failed to find server binary: %1"
msgstr ""

#: lspclientservermanager.cpp:864 lspclientservermanager.cpp:906
#, kde-format
msgid "Please check your PATH for the binary"
msgstr ""

#: lspclientservermanager.cpp:865 lspclientservermanager.cpp:907
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""

#: lspclientservermanager.cpp:903
#, kde-format
msgid "Failed to start server: %1"
msgstr ""

#: lspclientservermanager.cpp:911
#, kde-format
msgid "Started server %2: %1"
msgstr "Стартиран сървър %2: %1"

#: lspclientservermanager.cpp:945
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""

#: lspclientservermanager.cpp:948
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr ""

#: lspclientservermanager.cpp:952
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr ""

#: lspclientsymbolview.cpp:240
#, kde-format
msgid "Symbol Outline"
msgstr ""

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Tree Mode"
msgstr "Йерархичен преглед"

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Автоматично разгъване на йерархията"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Sort Alphabetically"
msgstr "Сортиране по азбучен ред"

#: lspclientsymbolview.cpp:284
#, kde-format
msgid "Show Details"
msgstr "Показване на детайли"

#: lspclientsymbolview.cpp:445
#, kde-format
msgid "Symbols"
msgstr "Символи"

#: lspclientsymbolview.cpp:576
#, kde-format
msgid "No LSP server for this document."
msgstr "Няма LSP сървър  за този документ"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr "Настройки на клиент"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, fuzzy, kde-format
#| msgid "Format"
msgid "Format on save"
msgstr "Формат"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr "Активиране на семантично акцентиране"

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Completions:"
msgstr "Завършвания:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""
"Автоматично добавяне на импортирани данни, ако е необходимо, след завършване"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr "Навигация:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr "Показване на информация за символа,  посочен с мишката"

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr "Акцентиране на целевата линия при прескачане до нея"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, kde-format
msgid "Server:"
msgstr "Сървър:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, fuzzy, kde-format
#| msgctxt "@title:tab"
#| msgid "Diagnostics"
msgid "Show program diagnostics"
msgstr "Диагностика"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr "Показване на известия от сървъра LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr "Инкрементално синхронизиране на документи със сървъра на LSP"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr "Конспект на документа:"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Сортиране на символите по азбучен ред"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, kde-format
msgid "Display additional details for symbols"
msgstr "Показване на допълнителни подробности за символите"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr "Представяне на символите в йерархия вместо в плосък списък"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, kde-format
msgid "Automatically expand tree"
msgstr "Автоматично разгъване на йерархията"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr "Потребителски настройки на сървър"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr "Файл с настройки:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr "Стандартни настройки на сървър"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "Повече опции"
