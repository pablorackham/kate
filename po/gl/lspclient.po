# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-08 00:49+0000\n"
"PO-Revision-Date: 2020-01-12 12:53+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.3\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:252
#, kde-format
msgid "Filter..."
msgstr "Filtrar…"

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:98 lspclientconfigpage.cpp:103
#: lspclientpluginview.cpp:471 lspclientpluginview.cpp:627 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "Cliente de LSP"

#: lspclientconfigpage.cpp:213
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: lspclientconfigpage.cpp:222
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: lspclientconfigpage.cpp:227
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: lspclientconfigpage.cpp:275
#, kde-format
msgid "Delete selected entries"
msgstr ""

#: lspclientconfigpage.cpp:280
#, kde-format
msgid "Delete all entries"
msgstr ""

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr ""

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:438 lspclientpluginview.cpp:691
#, kde-format
msgid "LSP"
msgstr ""

#: lspclientpluginview.cpp:499
#, kde-format
msgid "Go to Definition"
msgstr "Ir á definición"

#: lspclientpluginview.cpp:501
#, kde-format
msgid "Go to Declaration"
msgstr "Ir á declaración"

#: lspclientpluginview.cpp:503
#, fuzzy, kde-format
#| msgid "Go to Definition"
msgid "Go to Type Definition"
msgstr "Ir á definición"

#: lspclientpluginview.cpp:505
#, kde-format
msgid "Find References"
msgstr "Atopar referencias"

#: lspclientpluginview.cpp:508
#, kde-format
msgid "Find Implementations"
msgstr ""

#: lspclientpluginview.cpp:510
#, kde-format
msgid "Highlight"
msgstr "Realzar"

#: lspclientpluginview.cpp:512
#, fuzzy, kde-format
#| msgid "Symbols"
msgid "Symbol Info"
msgstr "Símbolos"

#: lspclientpluginview.cpp:514
#, kde-format
msgid "Search and Go to Symbol"
msgstr ""

#: lspclientpluginview.cpp:519
#, kde-format
msgid "Format"
msgstr "Formatar"

#: lspclientpluginview.cpp:522
#, kde-format
msgid "Rename"
msgstr "Renomear"

#: lspclientpluginview.cpp:526
#, fuzzy, kde-format
#| msgid "Expand All"
msgid "Expand Selection"
msgstr "Expandilo todo"

#: lspclientpluginview.cpp:529
#, kde-format
msgid "Shrink Selection"
msgstr ""

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Switch Source Header"
msgstr ""

#: lspclientpluginview.cpp:536
#, fuzzy, kde-format
#| msgid "Expand All"
msgid "Expand Macro"
msgstr "Expandilo todo"

#: lspclientpluginview.cpp:538
#, fuzzy, kde-format
#| msgid "General Options"
msgid "Code Action"
msgstr "Opcións xerais"

#: lspclientpluginview.cpp:553
#, kde-format
msgid "Show selected completion documentation"
msgstr "Amosar a documentación do completado seleccionado"

#: lspclientpluginview.cpp:556
#, kde-format
msgid "Enable signature help with auto completion"
msgstr ""

#: lspclientpluginview.cpp:559
#, kde-format
msgid "Include declaration in references"
msgstr "Incluír a declaración nas referencias"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:562 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr ""

#: lspclientpluginview.cpp:565
#, kde-format
msgid "Show hover information"
msgstr "Amosar información ao cubrir"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:568 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr "Formatar ao escribir"

#: lspclientpluginview.cpp:571
#, kde-format
msgid "Incremental document synchronization"
msgstr "Sincronización incremental dos documentos"

#: lspclientpluginview.cpp:574
#, kde-format
msgid "Highlight goto location"
msgstr ""

#: lspclientpluginview.cpp:583
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:587
#, fuzzy, kde-format
#| msgid "Show diagnostics notifications"
msgid "Show Diagnostics Notifications"
msgstr "Mostrar notificacións do diagnóstico"

#: lspclientpluginview.cpp:592
#, fuzzy, kde-format
#| msgid "Show Details"
msgid "Show Messages"
msgstr "Mostrar os detalles"

#: lspclientpluginview.cpp:597
#, kde-format
msgid "Server Memory Usage"
msgstr ""

#: lspclientpluginview.cpp:601
#, fuzzy, kde-format
#| msgid "Close all non-diagnostics tabs"
msgid "Close All Dynamic Reference Tabs"
msgstr "Pechar os separadores que non sexan de diagnóstico"

#: lspclientpluginview.cpp:603
#, kde-format
msgid "Restart LSP Server"
msgstr "Reiniciar o servidor de LSP"

#: lspclientpluginview.cpp:605
#, kde-format
msgid "Restart All LSP Servers"
msgstr "Reiniciar todos os servidores de LSP"

#: lspclientpluginview.cpp:616
#, kde-format
msgid "Go To"
msgstr ""

#: lspclientpluginview.cpp:643
#, kde-format
msgid "More options"
msgstr ""

#: lspclientpluginview.cpp:859 lspclientsymbolview.cpp:287
#, kde-format
msgid "Expand All"
msgstr "Expandilo todo"

#: lspclientpluginview.cpp:860 lspclientsymbolview.cpp:288
#, kde-format
msgid "Collapse All"
msgstr "Pregalo todo"

#: lspclientpluginview.cpp:1090
#, kde-format
msgid "RangeHighLight"
msgstr "Realce de intervalo"

#: lspclientpluginview.cpp:1424
#, kde-format
msgid "Line: %1: "
msgstr "Liña: %1: "

#: lspclientpluginview.cpp:1576 lspclientpluginview.cpp:1932
#: lspclientpluginview.cpp:2052
#, kde-format
msgid "No results"
msgstr "Non hai resultados"

#: lspclientpluginview.cpp:1635
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Definición: %1"

#: lspclientpluginview.cpp:1641
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Declaración: %1"

#: lspclientpluginview.cpp:1647
#, fuzzy, kde-format
#| msgctxt "@title:tab"
#| msgid "Definition: %1"
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Definición: %1"

#: lspclientpluginview.cpp:1653
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Referencias: %1"

#: lspclientpluginview.cpp:1665
#, fuzzy, kde-format
#| msgctxt "@title:tab"
#| msgid "Definition: %1"
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Definición: %1"

#: lspclientpluginview.cpp:1678
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Realzar: %1"

#: lspclientpluginview.cpp:1702 lspclientpluginview.cpp:1713
#: lspclientpluginview.cpp:1726
#, fuzzy, kde-format
#| msgid "General Options"
msgid "No Actions"
msgstr "Opcións xerais"

#: lspclientpluginview.cpp:1717
#, kde-format
msgid "Loading..."
msgstr ""

#: lspclientpluginview.cpp:1809
#, kde-format
msgid "No edits"
msgstr "Non hai edicións"

#: lspclientpluginview.cpp:1890
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Renomear"

#: lspclientpluginview.cpp:1891
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "Novo nome (ollo: poden non substituírse todas as referencias)"

#: lspclientpluginview.cpp:1938
#, fuzzy, kde-format
#| msgid "No results"
msgid "Not enough results"
msgstr "Non hai resultados"

#: lspclientpluginview.cpp:2008
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr ""

#: lspclientpluginview.cpp:2204 lspclientpluginview.cpp:2241
#, fuzzy, kde-format
#| msgid "Restart LSP Server"
msgctxt "@info"
msgid "LSP Server"
msgstr "Reiniciar o servidor de LSP"

#: lspclientpluginview.cpp:2264
#, fuzzy, kde-format
#| msgid "LSP Client"
msgctxt "@info"
msgid "LSP Client"
msgstr "Cliente de LSP"

#: lspclientservermanager.cpp:652
#, kde-format
msgid "Restarting"
msgstr ""

#: lspclientservermanager.cpp:652
#, kde-format
msgid "NOT Restarting"
msgstr ""

#: lspclientservermanager.cpp:653
#, fuzzy, kde-format
#| msgid "Server terminated unexpectedly: %1"
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr "O servidor rematou inesperadamente: %1"

#: lspclientservermanager.cpp:861
#, fuzzy, kde-format
#| msgid "Failed to start server: %1"
msgid "Failed to find server binary: %1"
msgstr "Non se puido iniciar o servidor: %1"

#: lspclientservermanager.cpp:864 lspclientservermanager.cpp:906
#, kde-format
msgid "Please check your PATH for the binary"
msgstr ""

#: lspclientservermanager.cpp:865 lspclientservermanager.cpp:907
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""

#: lspclientservermanager.cpp:903
#, kde-format
msgid "Failed to start server: %1"
msgstr "Non se puido iniciar o servidor: %1"

#: lspclientservermanager.cpp:911
#, fuzzy, kde-format
#| msgid "Failed to start server: %1"
msgid "Started server %2: %1"
msgstr "Non se puido iniciar o servidor: %1"

#: lspclientservermanager.cpp:945
#, fuzzy, kde-format
#| msgid "Failed to parse server configuration: %1"
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr "Non se puido analizar a configuración do servidor: %1"

#: lspclientservermanager.cpp:948
#, fuzzy, kde-format
#| msgid "Failed to parse server configuration: %1"
msgid "Failed to parse server configuration '%1': %2"
msgstr "Non se puido analizar a configuración do servidor: %1"

#: lspclientservermanager.cpp:952
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr "Non se puido ler a configuración do servidor: %1"

#: lspclientsymbolview.cpp:240
#, fuzzy, kde-format
#| msgid "Symbol Outline Options"
msgid "Symbol Outline"
msgstr "Opcións do contorno dos símbolos"

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Tree Mode"
msgstr "Modo de árbore"

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Expandir a árbore automaticamente"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Sort Alphabetically"
msgstr "Ordenar alfabeticamente"

#: lspclientsymbolview.cpp:284
#, kde-format
msgid "Show Details"
msgstr "Amosar os detalles"

#: lspclientsymbolview.cpp:445
#, kde-format
msgid "Symbols"
msgstr "Símbolos"

#: lspclientsymbolview.cpp:576
#, kde-format
msgid "No LSP server for this document."
msgstr "Non hai un servidor de LSP para este documento."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, fuzzy, kde-format
#| msgid "Format on typing"
msgid "Format on save"
msgstr "Formatar ao escribir"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, fuzzy, kde-format
#| msgid "General Options"
msgid "Completions:"
msgstr "Opcións xerais"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, fuzzy, kde-format
#| msgid "Show selected completion documentation"
msgid "Show inline docs for selected completion"
msgstr "Mostrar a documentación do completado seleccionado"

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, fuzzy, kde-format
#| msgid "Restart LSP Server"
msgid "Server:"
msgstr "Reiniciar o servidor de LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, fuzzy, kde-format
#| msgid "Show diagnostics marks"
msgid "Show program diagnostics"
msgstr "Mostrar marcas do diagnóstico"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Ordenar os símbolos alfabeticamente"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, fuzzy, kde-format
#| msgid "Display symbol details"
msgid "Display additional details for symbols"
msgstr "Mostrar os detalles dos símbolos"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, fuzzy, kde-format
#| msgid "Automatically Expand Tree"
msgid "Automatically expand tree"
msgstr "Expandir a árbore automaticamente"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr ""

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, fuzzy, kde-format
#| msgid "General Options"
msgid "More Options"
msgstr "Opcións xerais"

#, fuzzy
#~| msgid "Show diagnostics highlights"
#~ msgid "Show Diagnostics Highlights"
#~ msgstr "Mostrar realces do diagnóstico"

#, fuzzy
#~| msgid "Show diagnostics marks"
#~ msgid "Show Diagnostics Marks"
#~ msgstr "Mostrar marcas do diagnóstico"

#, fuzzy
#~| msgid "Show diagnostics marks"
#~ msgid "Show Diagnostics on Hover"
#~ msgstr "Mostrar marcas do diagnóstico"

#, fuzzy
#~| msgid "Switch to diagnostics tab"
#~ msgid "Switch to Diagnostics Tab"
#~ msgstr "Ir ao separador de diagnóstico"

#~ msgctxt "@title:tab"
#~ msgid "Diagnostics"
#~ msgstr "Diagnóstico"

#~ msgid "Error"
#~ msgstr "Erro"

#~ msgid "Warning"
#~ msgstr "Aviso"

#~ msgid "Information"
#~ msgstr "Información"

#, fuzzy
#~| msgctxt "@title:tab"
#~| msgid "Diagnostics"
#~ msgid "Diagnostics:"
#~ msgstr "Diagnóstico"

#, fuzzy
#~| msgid "Show diagnostics marks"
#~ msgid "Show diagnostics on hover"
#~ msgstr "Mostrar marcas do diagnóstico"

#~ msgid "LSP Client Symbol Outline"
#~ msgstr "Contorno dos símbolos do cliente de LSP"

#~ msgid "General Options"
#~ msgstr "Opcións xerais"

#~ msgid "Add highlights"
#~ msgstr "Engadir realces"

#~ msgid "Add markers"
#~ msgstr "Engadir marcadores"

#, fuzzy
#~| msgid "Hover"
#~ msgid "On hover"
#~ msgstr "Pasar por riba"

#~ msgid "Tree mode outline"
#~ msgstr "Contorno en modo de árbore"

#~ msgid "Automatically expand nodes in tree mode"
#~ msgstr "Expandir automaticamente os nodos no modo en árbore"

#, fuzzy
#~| msgid "Error"
#~ msgctxt "@info"
#~ msgid "Error"
#~ msgstr "Erro"

#, fuzzy
#~| msgid "Warning"
#~ msgctxt "@info"
#~ msgid "Warning"
#~ msgstr "Aviso"

#, fuzzy
#~| msgid "Information"
#~ msgctxt "@info"
#~ msgid "Information"
#~ msgstr "Información"

#, fuzzy
#~| msgid "Switch to diagnostics tab"
#~ msgid "Switch to messages tab"
#~ msgstr "Ir ao separador de diagnóstico"

#~ msgid "Hover"
#~ msgstr "Pasar por riba"

#~ msgctxt "@info"
#~ msgid "<b>LSP Client:</b> %1"
#~ msgstr "<b>Cliente de LSP:</b> %1"

#~ msgid "Server Configuration"
#~ msgstr "Configuración do servidor"

#, fuzzy
#~| msgid "Format"
#~ msgid "Form"
#~ msgstr "Formatar"
